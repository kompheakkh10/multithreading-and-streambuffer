package javaio;

import java.io.*;

public class StreamBufferLearning {
    public static void main(String[] args) throws IOException {
        //create new file output stream
        OutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(
                        "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text2.txt"
                )
        );

        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text2.txt"
        );

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        String text = "Hello World";
        byte[] b = text.getBytes();

        bufferedOutputStream.write(b);
        bufferedOutputStream.flush();
        bufferedOutputStream.close();
        fileOutputStream.close();
        System.out.println("Success that write into the file!");
    }
}
