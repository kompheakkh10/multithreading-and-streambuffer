package javaio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.SequenceInputStream;

public class InputStreamExample {
    public static void main(String[] args) throws IOException {
        FileInputStream firstFileInputStream = new FileInputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text2.txt"
        );
        FileInputStream secondFileInputStream = new FileInputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text.txt"
        );

        SequenceInputStream inputStream = new SequenceInputStream(firstFileInputStream, secondFileInputStream);
        int j;

        while ((j=inputStream.read())!=-1){
            System.out.print((char) j);
        }
        inputStream.close();
        firstFileInputStream.close();
        secondFileInputStream.close();
    }
}
