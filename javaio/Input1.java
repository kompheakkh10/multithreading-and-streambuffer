package javaio;

import java.io.*;

public class Input1 {
    public static void main(String[] args) throws IOException {
        FileInputStream thirdFileInputStream;
        //create file input stream for read data from the text file.
        FileInputStream firstFileInputStream = new FileInputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text.txt"
        );
        FileInputStream secondFileInputStream = new FileInputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text2.txt"
        );
        //create file out stream for create a file with new name and write data into it
        FileOutputStream fileOutputStream = new FileOutputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text3.txt"
        );

        SequenceInputStream sequenceInputStream = new SequenceInputStream(
                firstFileInputStream,
                secondFileInputStream
        );
        int i;
        while((i=sequenceInputStream.read())!=-1){
            fileOutputStream.write(i);
        }
        thirdFileInputStream = new FileInputStream(
                "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text3.txt"
        );
        BufferedInputStream bufferedInputStream = new BufferedInputStream(thirdFileInputStream);
        int j;
        // for break the line
        System.out.println("read From new file right now!");
        while((j=bufferedInputStream.read())!=-1){
            System.out.print((char) j);
        }

        // closed the stream buffer
        sequenceInputStream.close();
        firstFileInputStream.close();
        secondFileInputStream.close();
        fileOutputStream.close();
    }
}
