package javaio;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class InputStreamBuffer {
    public static void main(String[] args) {
        try{
            FileInputStream fileInputStream = new FileInputStream(
                    "D:\\DesktopFile\\Multithreading And Streambuffer\\multithreading-and-streambuffer\\javaio\\File Text2.txt"
            );
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            int i;

            while((i = bufferedInputStream.read())!=-1){
                System.out.print((char) i);
            }
            bufferedInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
